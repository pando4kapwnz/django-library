   pip3 install -r requirements.txt
   
   python3 manage.py makemigrations

   python3 manage.py migrate

   python3 manage.py collectstatic

   python3 manage.py test

   python3 manage.py createsuperuser # Create a superuser

   python3 manage.py runserver
   
Open a browser to `http://127.0.0.1:8000/admin/` to open the admin site

Open tab to `http://127.0.0.1:8000` to see the main site
